import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { VideosListagemComponent } from './videos-listagem/videos-listagem.component';
import { ListagemService } from './listagem.service';
import { HttpClientModule } from '@angular/common/http';
import { DetalhesComponent } from './detalhes/detalhes.component';



@NgModule({
  declarations: [
    AppComponent,
    VideosListagemComponent,
    DetalhesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,

  ],
  providers: [HttpClientModule, ListagemService],
  bootstrap: [AppComponent]
})
export class AppModule { }
