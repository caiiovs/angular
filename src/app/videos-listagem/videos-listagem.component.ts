import { Component, OnInit } from '@angular/core';
import { ListagemService } from "../listagem.service";


@Component({
  selector: 'app-videos-listagem',
  templateUrl: './videos-listagem.component.html',
  styleUrls: ['./videos-listagem.component.css']
})
export class VideosListagemComponent implements OnInit {

  dados: Array<any>;
  dadosEmbed: Array<any>;
  constructor(private listagemService: ListagemService) { }

  ngOnInit(): void {
  }

  listarVideos() {
    var value = document.getElementById('search-film-input').value;
    if(value == ""){
      alert("por favor, preencha o campo pesquisa!");
    }else{
      this.listagemService.listar(value).subscribe(rest => this.dados = rest);
      document.getElementById('results').style.display = 'block';
    }
  }

  showDetails(videoId){
    this.listagemService.listarDetalhe(videoId).subscribe(rest => this.dadosEmbed = rest);
    document.getElementById("videoId").value = this.dadosEmbed.items[0].id;
    document.getElementById("titulo").innerText = this.dadosEmbed.items[0].snippet.title;
    document.getElementById("likeSpan").innerText = this.dadosEmbed.items[0].statistics.likeCount;
    document.getElementById("dislikeSpan").innerText = this.dadosEmbed.items[0].statistics.dislikeCount;


    document.getElementById('detalhes').style.display = 'block';
    document.getElementById('search-film').style.display = 'none';
    document.getElementById('results').style.display = 'none';
  }

}
