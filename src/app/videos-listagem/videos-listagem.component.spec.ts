import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideosListagemComponent } from './videos-listagem.component';

describe('VideosListagemComponent', () => {
  let component: VideosListagemComponent;
  let fixture: ComponentFixture<VideosListagemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideosListagemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideosListagemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
