import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-app';

  showList() {
    var value = document.getElementById('search-film-input').value;

    if(value == ""){
      alert("por favor, preencha o campo pesquisa");
    }else{
      document.getElementById('results').style.display = 'block';
    }
  }

}


