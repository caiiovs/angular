import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ListagemService {

  constructor(private http: HttpClient) { }

  listar (value) {
    return this.http.get("https://www.googleapis.com/youtube/v3/search?part=id,snippet&q="+value+"&key=AIzaSyD6gsZMt5sQseLL9QpWKK13T6Kcdh0hMAQ");
  }

  listarDetalhe (value) {
    return this.http.get("https://www.googleapis.com/youtube/v3/videos?id="+value+"&part=snippet,statistics&key=AIzaSyD6gsZMt5sQseLL9QpWKK13T6Kcdh0hMAQ");
  }
}
